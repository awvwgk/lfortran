-- This is an Abstract Syntax Description Lanuguage (ASDL) that describes the
-- Fortran's Abstract Syntax Tree (AST). See [1] for a short background
-- information and motivation and see the paper [2] for a detailed motivation,
-- explanation and specification of the language. See [3] for further examples
-- of ASDL usage in a real compiler.
--
-- [1] https://eli.thegreenplace.net/2014/06/04/using-asdl-to-describe-asts-in-compilers/
-- [2] Wang, D. C., Appel, A. W., Korn, J. L., & Serra, C. S. (1997). The Zephyr Abstract Syntax Description Language. USENIX Conference on Domain-Specific Languages, (October), 213–228.
-- [3] https://arxiv.org/abs/cs/9810013
--
-- ASDL's builtin types are:
--   * identifier
--   * int (signed integers of infinite precision)
--   * string
-- We extend these by:
--   * node (any ASDL node)
--   * bool
--
-- Specific tools may choose to produce language interfaces that represent
-- these types and the ASDL tree in a language specific way (e.g. use finite
-- precision integers and signal an error otherwise).
--
-- At the AST level we strictly only represent local syntax (no semantics). That
-- way each syntax construct is parsed locally, no need to do any non-local
-- lookups to figure out what AST node to construct. That is the job of the AST
-- to ASR conversion to do non-local lookups to figure out what is what (i.e.,
-- disambiguate) and report any errors.

module AST {

unit
    = TranslationUnit(node* items)

mod
    = Module(identifier name, unit_decl1* use, implicit_statement* implicit,
        unit_decl2* decl, program_unit* contains)
    | Submodule(identifier id, identifier? parent_name, identifier name,
        unit_decl1* use, implicit_statement* implicit,
        unit_decl2* decl, program_unit* contains)
    | BlockData(identifier? name, unit_decl1* use,
        implicit_statement* implicit, unit_decl2* decl)
    | Program(identifier name, unit_decl1* use, implicit_statement* implicit,
        unit_decl2* decl, stmt* body, program_unit* contains)

program_unit
    = Subroutine(identifier name, arg* args, decl_attribute* attributes,
        bind? bind, unit_decl1* use, import_statement* import,
        implicit_statement* implicit, unit_decl2* decl,
        stmt* body, program_unit* contains)
    | Procedure(identifier name, arg* args, decl_attribute* attributes,
        unit_decl1* use, import_statement* import, implicit_statement* implicit,
        unit_decl2* decl, stmt* body, program_unit* contains)
    | Function(identifier name, arg* args, decl_attribute* attributes,
        expr? return_var, bind? bind, unit_decl1* use, import_statement* import,
        implicit_statement* implicit, unit_decl2* decl, stmt* body,
        program_unit* contains)

unit_decl1
    = Use(decl_attribute* nature, identifier module, use_symbol* symbols,
        bool only_present)

unit_decl2
    = Declaration(decl_attribute? vartype, decl_attribute* attributes,
        var_sym* syms)
    | Interface(interface_header header, interface_item* items)
    | DerivedType(identifier name, decl_attribute* attrtype,
        unit_decl2* items, procedure_decl* contains)
    | Enum(decl_attribute* attr, unit_decl2* items)

interface_header
    = InterfaceHeader()
    | InterfaceHeaderName(identifier name)
    | InterfaceHeaderAssignment()
    | InterfaceHeaderOperator(intrinsicop op)
    | InterfaceHeaderDefinedOperator(string operator_name)
    | AbstractInterfaceHeader()

interface_item
    = InterfaceProc(program_unit proc)
    | InterfaceModuleProcedure(identifier* names, decl_attribute* attributes)

import_statement
    = Import(identifier* symbols, import_modifier mod)

import_modifier
    = ImportDefault
    | ImportOnly
    | ImportNone
    | ImportAll

implicit_statement
    = ImplicitNone(implicit_none_spec* specs)
    | Implicit(decl_attribute type, letter_spec* kind, letter_spec* specs)

implicit_none_spec
    = ImplicitNoneExternal(int dummy)
    | ImplicitNoneType()

letter_spec
    = LetterSpec(identifier? start, identifier end)

stmt
-- Single-line statements (each has a `label`):
    = Allocate(int label, fnarg* args, keyword* keywords)
    | Assignment(int label, expr target, expr value)
    | Associate(int label, expr target, expr value)
    | Backspace(int label, expr* args, keyword* kwargs)
    | Close(int label, expr* args, keyword* kwargs)
    | Continue(int label)
    | Cycle(int label, identifier? stmt_name)
    | Deallocate(int label, fnarg* args, keyword* keywords)
    | ErrorStop(int label, expr? code, expr? quiet)
    | EventPost(int label, expr variable, event_attribute* stat)
    | EventWait(int label, expr variable, event_attribute* spec)
    | Exit(int label, identifier? stmt_name)
    | Flush(int label, expr* args, keyword* kwargs)
    | ForAllSingle(int label, identifier? stmt_name,
            concurrent_control *control, expr? mask, stmt assign)
    | Format(int label, string fmt)
    | GoTo(int label, expr goto_label, expr* labels)
    | Inquire(int label, expr* args, keyword* kwargs, expr* values)
    | Nullify(int label, expr* args, keyword* kwargs)
    | Open(int label, expr* args, keyword* kwargs)
    | Return(int label, expr? value)
    | Print(int label, string? fmt, expr* values)
    | Read(int label, argstar* args, kw_argstar* kwargs, expr* values)
    | Rewind(int label, expr* args, keyword* kwargs)
    | Stop(int label, expr? code, expr? quiet)
    | SubroutineCall(int label, identifier name, struct_member* member,
            fnarg* args, keyword* keywords)
    | SyncAll(int label, event_attribute* stat)
    | Write(int label, argstar* args, kw_argstar* kwargs, expr* values)

-- Multi-line statements (each has a `label` and `stmt_name`):
    | AssociateBlock(int label, identifier? stmt_name, var_sym* syms,
            stmt* body)
    | Block(int label, identifier? stmt_name, unit_decl1* use,
            import_statement* import,unit_decl2* decl, stmt* body)
    | Critical(int label, identifier? stmt_name, event_attribute* sync_stat,
            stmt* body)
    | DoConcurrentLoop(int label, identifier? stmt_name,
            concurrent_control *control, expr? mask,
            concurrent_locality* locality, stmt* body)
    | DoLoop(int label, identifier? stmt_name, int do_label, identifier? var,
            expr? start, expr? end, expr? increment, stmt* body)
    | ForAll(int label, identifier? stmt_name,
            concurrent_control *control, expr? mask,
            concurrent_locality* locality, stmt* body)
    | If(int label, identifier? stmt_name, expr test, stmt* body, stmt* orelse)
    | Select(int label, identifier? stmt_name, expr test, case_stmt* body)
    | SelectRank(int label, identifier? stmt_name, identifier? assoc_name,
            expr selector, rank_stmt* body)
    | SelectType(int label, identifier? stmt_name, identifier? assoc_name,
            expr selector, type_stmt* body)
    | Where(int label, identifier? stmt_name, expr test, stmt* body,
            stmt* orelse)
    | WhileLoop(int label, identifier? stmt_name, expr test, stmt* body)

expr
    = BoolOp(expr left, boolop op, expr right)
    | BinOp(expr left, operator op, expr right)
    | DefBinOp(expr left, string op, expr right)
    | StrOp(expr left, stroperator op, expr right)
    | UnaryOp(unaryop op, expr operand)
    | Compare(expr left, cmpop op, expr right)
    | FuncCallOrArray(identifier func, struct_member* member,
        fnarg* args, keyword* keywords, fnarg* subargs)
    | CoarrayRef(identifier name, struct_member* member,
        fnarg* args, keyword* fnkw, coarrayarg* coargs, keyword* cokw)
    | ArrayInitializer(decl_attribute? vartype, expr* args)
    | ImpliedDoLoop(expr* values, identifier var, expr start, expr end,
        expr? increment)
    | Num(int n, string? kind)
    | Real(string n)
    | Complex(expr re, expr im)
    | String(string s)
    | BOZ(string s)
    | Name(identifier id, struct_member* member)
    | Logical(bool value)
    | DataImpliedDo(expr* object_list, decl_attribute? type,
        identifier var, expr start, expr end, expr? increment)

boolop = And | Or | Eqv | NEqv

operator = Add | Sub | Mul | Div | Pow

stroperator = Concat

unaryop = Invert | Not | UAdd | USub

cmpop = Eq | NotEq | Lt | LtE | Gt | GtE

intrinsicop
    = AND | OR | EQV | NEQV
    | PLUS | MINUS | STAR | DIV | POW
    | NOT
    | EQ | NOTEQ | LT | LTE | GT | GTE

procedure_decl
    = DerivedTypeProc(identifier? name, decl_attribute* attr,
        use_symbol* symbols)
    | GenericOperator(decl_attribute* attr, intrinsicop op, identifier* names)
    | GenericDefinedOperator(decl_attribute* attr, string optype,
        identifier* names)
    | GenericAssignment(decl_attribute* attr, identifier* names)
    | GenericName(decl_attribute* attr, identifier name, identifier* names)
    | FinalName(identifier name)

decl_attribute
    = AttrBind(identifier name)
    | AttrData(expr* object, expr* value)
    | AttrDimension(dimension* dim)
    | AttrCodimension(codimension* codim)
    | AttrEquivalence(equi* args)
    | AttrExtends(identifier name)
    | AttrIntent(attr_intent intent)
    | AttrNamelist(identifier name)
    | AttrPass(identifier? name)
    | SimpleAttribute(simple_attribute attr)
    | AttrType(decl_type type, kind_item* kind, identifier? name)
    | AttrAssignment()
    | AttrIntrinsicOperator(intrinsicop op)
    | AttrDefinedOperator(string op_name)

simple_attribute
    = AttrAbstract
    | AttrAllocatable
    | AttrAsynchronous
    | AttrCommon
    | AttrContiguous
    | AttrDeferred
    | AttrElemental
    | AttrEnumerator
    | AttrExternal
    | AttrImpure
    | AttrIntrinsic
    | AttrModule
    | AttrNoPass
    | AttrNonDeferred
    | AttrNon_Intrinsic
    | AttrOptional
    | AttrParameter
    | AttrPointer
    | AttrPrivate
    | AttrProtected
    | AttrPublic
    | AttrPure
    | AttrRecursive
    | AttrSave
    | AttrSequence
    | AttrTarget
    | AttrValue
    | AttrVolatile

attr_intent = In | Out | InOut

decl_type
    = TypeClass
    | TypeCharacter
    | TypeComplex
    | TypeDoublePrecision
    | TypeInteger
    | TypeLogical
    | TypeProcedure
    | TypeReal
    | TypeType

event_attribute
    = AttrStat(identifier variable)
    | AttrErrmsg(identifier variable)
    | AttrEventWaitKwArg(identifier id, expr value)

var_sym = (identifier? name, dimension* dim, codimension* codim,
 expr? initializer, symbol sym, decl_attribute? spec)

kind_item = (identifier? id, expr? value, kind_item_type type)

kind_item_type = Star | Colon | Value

dimension = (expr? start, expr? end, dimension_type end_star)
dimension_type = DimensionExpr | DimensionStar | AssumedRank

codimension = (expr? start, expr? end, codimension_type end_star)
codimension_type = CodimensionExpr | CodimensionStar

symbol = None | Assign | Equal | Asterisk

equi = (expr* set_list)

-- Encoding of an array dimension declaration:
--           start      end     end_star
-- Declaration:
-- X(n)       1          n       Expr   # Note: X(n) is equivalent to X(1:n)
-- X(:)       ()         ()      Expr
-- X(a:)      a          ()      Expr
-- X(:b)      ()         b       Expr
-- X(a:b)     a          b       Expr
-- X(*)       ()         ()      Star
-- X(a:*)     a          ()      Star


-- Null for `expr` means it's a star
kw_argstar = (identifier arg, expr? value)
argstar = (expr? value)

-- The identifier is wrapped up in a product type so that location information
-- is included:
arg = (identifier arg)


-- Encoding of array elements and sections in fnarg:
--           start      end     step
-- element:
-- X(i)       ()         i       ()
-- section:
-- X(:)       ()         ()      1
-- X(a:)      a          ()      1
-- X(:b)      ()         b       1
-- X(a:b)     a          b       1
-- X(::c)     ()         ()      c
-- X(a::c)    a          ()      c
-- X(:b:c)    ()         b       c
-- X(a:b:c)   a          b       c
--
fnarg = (expr? start, expr? end, expr? step)

coarrayarg = (expr? start, expr? end, expr? step, codimension_type star)

keyword = (identifier arg, expr value)

struct_member = (identifier name, fnarg* args)

bind = Bind(expr* args, keyword* kwargs)

array_index = ArrayIndex(expr? left, expr? right, expr? step)

case_stmt
    = CaseStmt(case_cond* test, stmt* body)
    | CaseStmt_Default(stmt* body)

case_cond
    = CaseCondExpr(expr cond)
    | CaseCondRange(expr? start, expr? end)

rank_stmt
    = RankExpr(expr value, stmt* body)
    | RankStar(stmt* body)
    | RankDefault(stmt* body)

type_stmt
    = TypeStmtName(identifier? name, stmt* body)
    | TypeStmtType(decl_attribute? vartype, stmt* body)
    | ClassStmt(identifier? id, stmt* body)
    | ClassDefault(stmt* body)

use_symbol
    = UseSymbol(identifier sym, identifier? rename)
    | UseAssignment()
    | IntrinsicOperator(intrinsicop op)
    | DefinedOperator(string opName)
    | RenameOperator(string local_defop, string use_defop)

concurrent_control = ConcurrentControl(identifier? var, expr? start, expr? end, expr? increment)

concurrent_locality
    = ConcurrentLocal(identifier *vars)
    | ConcurrentLocalInit(identifier *vars)
    | ConcurrentShared(identifier *vars)
    | ConcurrentDefault()
    | ConcurrentReduce(reduce_op op, identifier *vars)

reduce_op = ReduceAdd | ReduceMul | ReduceMIN | ReduceMAX

}
